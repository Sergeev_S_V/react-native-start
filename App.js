import React, {Component} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';
import PlaceItem from "./src/components/PlaceItem";

class App extends Component {
  state = {
    placeName: '',
    places: [],
    something: true
  };

  placeNameChangedHandler = text => {
    this.setState({placeName: text});
  };

  placeAddedHandler = () => {
    this.setState(prevState => {
      return {
        places: prevState.places.concat(prevState.placeName),
        placeName: ''
      }
    });
  };

  placePressedHandler = () => {
    alert('Place was pressed!');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="Enter place name"
            style={[styles.input, styles.largeText]}
            value={this.state.placeName}
            onChangeText={this.placeNameChangedHandler}
          />
          <Button style={{width: '20%'}} title="Add" onPress={this.placeAddedHandler} />
        </View>
        <View style={styles.itemsList}>
          {this.state.places.map((place, i) => (
            <PlaceItem key={i} placeName={place} placePressed={this.placePressedHandler} />
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 30
  },
  input: {
    width: '80%',
    height: 40
  },
  largeText: {
    fontSize: 20
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  itemsList: {
    width: '100%'
  }
});

export default App;